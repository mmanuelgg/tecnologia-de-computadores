.data
tam: .word 5
datos: .word 1, 2, 3, 4, 5

.text
.global main
main:
	ldr r0, =tam
	ldr r0, [r0]
	ldr r1, =datos
	mov r2, #0
	for:
		cmp r2, r0
		bge sal
		ldr r3, [r1]
		lsl r3, r3, #1
		str r3, [r1], #4
		add r2, r2, #1
		b for
	sal:
		bx lr