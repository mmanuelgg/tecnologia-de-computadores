.data
tam: .word 8
datos: .word 8,3,4,2,5,7,3,1
resul: .word 0,0,0,0,0,0,0,0
.text
.global main
main:
	ldr r0, =tam
	ldr r0, [r0]
	ldr r1, =datos
	ldr r2, =resul
	mov r3, #0
	mov r4, #0

	initR2:
		cmp r4, r0
		beq bucle
		add r2, r2, #4
		add r4, r4, #1
		b initR2

	bucle:
		cmp r3, r0
		bge salir
		ldr r5, [r1], #4
		str r5, [r2]
		sub r2, r2, #4
		add r3, r3, #1
		b bucle

	salir: bx lr