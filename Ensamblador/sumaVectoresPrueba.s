.data
tam:	.word 5
datos:	.word 2, 4, -6, 2, 2
res:	.word 0

.text
.global main
main:
	ldr r0, =tam
	ldr r1, [r0]
	ldr r2, =datos
	mov r3, #0
	bucle:
		cmp r1, #0
		beq sal
		ldr r4, [r2], #4
		add r3, r3, r4
		sub r1, #1
		b bucle
	sal:
		ldr r0, =res
		str r3, [r0]
	bx lr