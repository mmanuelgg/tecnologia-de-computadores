@Programa que dado un vector "datos" de tamaño "tam" cuente el numero de veces que aparece el valor "val" y almacenar el resultado en "res"
.data
tam: .word 5
datos: .word 1, 2, 1, 1, 3
val: .word 1
res: .word 0

.text
main:
	ldr r0, =tam
	ldr r0, [r0]
	ldr r1, =datos
	ldr r2, =val
	ldr r2, [r2]
	mov r3, #0
	mov r5, #0
	for:
		cmp r3, r0
		bge sal
		ldr r4, [r1], #4
		cmp r4, r2
		addeq r5, r5, #1
		add r3,r3, #1
		b for
	sal:
		ldr r6, =res
		str r5, [r6]
		bx lr