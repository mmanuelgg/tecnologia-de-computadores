/*
int main(){
  int datos[5] = {1,2,3,4,5};
  int len = 5;
  int out[5];

  cuentaPares(datos, out, len);
  return 0;
}
int esPar(int num){
  if(num%2)
    return 0;

  return 1;
}
void cuentaPares(int in[], int out[], int len){
  for (int i=0; i<len; i++){
    int j = in[i];
    int k = esPar(j);
    out[i] = k;
  }
}

*/

.data
datos: .word 1,2,3,4,5
len: .word 5
out: .word 0,0,0,0,0
.text
.global main
main:
	push {lr}
	ldr r0, = datos
	ldr r1, = out
	ldr r2, = len
	ldr r2, [r2]
	bl cueentaPares
	pop {lr}
	bx lr
	
espar:
	and r0, r0, #1
	bx lr
	
cuentaPares:
	push {r4-r6, lr}
	mov r3, #0
	for:
		cmp r3, r2
		bge salir
		lsl r6, r3, #2	@i desplazado 2 lugares hacia izq->multiplicar por 4
		add r6, r6, r0
		ldr r4, [r6]		@ldr r4, [r0], #4
		push {r0}
		push {r1-r3}
		mov r0, r4
		bl espar
		pop {r1-r3}
		lsl r6, r3, #2
		add r6, r6, r1
		str r0, [r6]
		pop {r0}
		add r3, r3, #1
		b for
		
salir:
		pop {r4-r6, lr}
		bx lr
		