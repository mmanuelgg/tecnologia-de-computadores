.data
n:	.word 1
res:	.word 0

.text
main:
	ldr r0, =n
	ldr r0, [r0]
	push {lr}
	bl fib
	pop {lr}
	bx lr
	
fib:
	mov r1, #0
	mov r2, #1
	mov r3, #0
	
	bucle:
		cmp r0, #0
		beq esCero
		cmp r0, #1
		ble sal
		add r3, r3, r1
		add r3, r3, r2
		mov r1, r2
		mov r2, r3
		sub r3, r3, r3
		sub r0, r0, #1
		b bucle
	sal:
		mov r0, r2
		mov pc, lr
	esCero:
		mov pc, lr