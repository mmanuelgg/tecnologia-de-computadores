.data
notes: .word C,Db,D,Eb,E,F,Gb,G,Ab,A,Bb,B,CP,DbP,DP,EbP,EP,FP,GbP,GP,AbP,AP,BbP,BP,SILEN
c: .word C
db: .word Db
d: .word D
eb: .word Eb
e: .word E
f: .word F
gb: .word Gb
g: .word G
ab: .word Ab
a: .word A
bb: .word Bb
b: .word B
cp: .word CP
dbp: .word DbP
dp: .word DP
ebp: .word EbP
ep: .word EP
fp: .word FP
gbp: .word GbP
gp: .word GP
abp: .word AbP
ap: .word AP
bbp: .word BbP
bp: .word BP
silen: .word SILEN
length: .word NG,CORP,COR,FUS,BL
ng: .word NG
corp: .word CORP
cor: .word COR
fus: .word FUS
bl: .word BL
.include "wiringPiNotes.s"

.text
.global main
main:
	push {r4-r6,lr}
	bl initBerry
	
	ldr r0, =d
	ldr r0, [r0]
	ldr r1, =corp
	ldr r1, [r1]
	bl playNote
	
	ldr r0, =silen
	ldr r0, [r0]
	ldr r1, =fus
	ldr r1, [r1]
	bl playNote
	
	ldr r0, =d
	ldr r0, [r0]
	ldr r1, =corp
	ldr r1, [r1]
	bl playNote
	
	ldr r0, =a
	ldr r0, [r0]
	ldr r1, =corp
	ldr r1, [r1]
	bl playNote
	
	ldr r0, =silen
	ldr r0, [r0]
	ldr r1, =fus
	ldr r1, [r1]
	bl playNote
	
	ldr r0, =a
	ldr r0, [r0]
	ldr r1, =corp
	ldr r1, [r1]
	bl playNote
	
	ldr r0, =e
	ldr r0, [r0]
	ldr r1, =corp
	ldr r1, [r1]
	bl playNote
	
	ldr r0, =silen
	ldr r0, [r0]
	ldr r1, =fus
	ldr r1, [r1]
	bl playNote
	
	ldr r0, =f
	ldr r0, [r0]
	ldr r1, =cor
	ldr r1, [r1]
	bl playNote
	
	ldr r0, =e
	ldr r0, [r0]
	ldr r1, =corp
	ldr r1, [r1]
	bl playNote
	
	ldr r0, =d
	ldr r0, [r0]
	ldr r1, =bl
	ldr r1, [r1]
	bl playNote
	
	ldr r0, =silen
	ldr r0, [r0]
	ldr r1, =fus
	ldr r1, [r1]
	bl playNote
	
	ldr r0, =a
	ldr r0, [r0]
	ldr r1, =ng
	ldr r1, [r1]
	bl playNote
	
	ldr r0, =cp
	ldr r0, [r0]
	ldr r1, =ng
	ldr r1, [r1]
	bl playNote
	
	ldr r0, =dp
	ldr r0, [r0]
	ldr r1, =ng
	ldr r1, [r1]
	bl playNote
	
	ldr r0, =silen
	ldr r0, [r0]
	ldr r1, =fus
	ldr r1, [r1]
	bl playNote
	
	ldr r0, =cp
	ldr r0, [r0]
	ldr r1, =corp
	ldr r1, [r1]
	bl playNote
	
	ldr r0, =a
	ldr r0, [r0]
	ldr r1, =corp
	ldr r1, [r1]
	bl playNote
	
	ldr r0, =b
	ldr r0, [r0]
	ldr r1, =corp
	ldr r1, [r1]
	bl playNote
	
	ldr r0, =g
	ldr r0, [r0]
	ldr r1, =corp
	ldr r1, [r1]
	bl playNote
	
	ldr r0, =a
	ldr r0, [r0]
	ldr r1, =ng
	ldr r1, [r1]
	bl playNote
	
	ldr r0, =silen
	ldr r0, [r0]
	ldr r1, =fus
	ldr r1, [r1]
	bl playNote
	
	ldr r0, =dp
	ldr r0, [r0]
	ldr r1, =corp
	ldr r1, [r1]
	bl playNote
	
	ldr r0, =silen
	ldr r0, [r0]
	ldr r1, =fus
	ldr r1, [r1]
	bl playNote
	
	ldr r0, =dp
	ldr r0, [r0]
	ldr r1, =corp
	ldr r1, [r1]
	bl playNote
	
	ldr r0, =silen
	ldr r0, [r0]
	ldr r1, =fus
	ldr r1, [r1]
	bl playNote
	
	ldr r0, =cp
	ldr r0, [r0]
	ldr r1, =corp
	ldr r1, [r1]
	bl playNote
	
	ldr r0, =silen
	ldr r0, [r0]
	ldr r1, =fus
	ldr r1, [r1]
	bl playNote
	
	ldr r0, =a
	ldr r0, [r0]
	ldr r1, =cor
	ldr r1, [r1]
	bl playNote
	
	ldr r0, =silen
	ldr r0, [r0]
	ldr r1, =fus
	ldr r1, [r1]
	bl playNote
	
	ldr r0, =a
	ldr r0, [r0]
	ldr r1, =ng
	ldr r1, [r1]
	bl playNote
	
	ldr r0, =g
	ldr r0, [r0]
	ldr r1, =ng
	ldr r1, [r1]
	bl playNote
	
	ldr r0, =f
	ldr r0, [r0]
	ldr r1, =ng
	ldr r1, [r1]
	bl playNote
	
	ldr r0, =e
	ldr r0, [r0]
	ldr r1, =ng
	ldr r1, [r1]
	bl playNote
	
	ldr r0, =silen
	ldr r0, [r0]
	ldr r1, =fus
	ldr r1, [r1]
	bl playNote
	
	ldr r0, =d
	ldr r0, [r0]
	ldr r1, =ng
	ldr r1, [r1]
	bl playNote
	
	ldr r0, =silen
	ldr r0, [r0]
	ldr r1, =fus
	ldr r1, [r1]
	bl playNote
	
	ldr r0, =a
	ldr r0, [r0]
	ldr r1, =corp
	ldr r1, [r1]
	bl playNote
	
	ldr r0, =g
	ldr r0, [r0]
	ldr r1, =ng
	ldr r1, [r1]
	bl playNote
	
	ldr r0, =silen
	ldr r0, [r0]
	ldr r1, =fus
	ldr r1, [r1]
	bl playNote
	
	ldr r0, =f
	ldr r0, [r0]
	ldr r1, =ng
	ldr r1, [r1]
	bl playNote
	
	ldr r0, =e
	ldr r0, [r0]
	ldr r1, =ng
	ldr r1, [r1]
	bl playNote
	
	ldr r0, =d
	ldr r0, [r0]
	ldr r1, =ng
	ldr r1, [r1]
	bl playNote
	
	ldr r0, =c
	ldr r0, [r0]
	ldr r1, =ng
	ldr r1, [r1]
	bl playNote
	
	ldr r0, =d
	ldr r0, [r0]
	ldr r1, =ng
	ldr r1, [r1]
	bl playNote
	
	pop {r4-r6,lr}
	bx lr
