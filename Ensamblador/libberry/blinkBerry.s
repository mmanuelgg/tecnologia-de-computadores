.data
leds:	.word RLED1, RLED2, YLED1, YLED2, GLED1, GLED2	/* id de los pines de los leds */

.include "wiringPiPins.s"  /* fichero con definiciones para el control de la placa */

.text
 
.global main
.func main
main:
/************************************************************************************/
/* Configuracion de la libreria wiringPi y los pines de los leds, botones y altavoz */
/************************************************************************************/

	bl initBerry		/* Inicializamos librería wiringPi y pins de la tarjeta de expansion */
	
bucle:					/* bucle para el parpadeo del led */
						/* escribimos un 1 en el pin */
						/* digitalWrite(int pin, int value) */
	mov r0, #RLED1		/* indicamos el pin */
	mov r1, #1			/* ponemos el valor 1 */
	bl digitalWrite		/* llamamos a la funcion digitalWrite */
						/* esperamos medio segundo */
						/* delay(int ms) */
	mov r0, #500		/* indicamos el numero de milisegundos */		
	bl delay			/* llamamos a la funcion delay */
						/* escribimos el valor 0 */
	mov r0, #RLED1		/* indicamos el pin */
	mov r1, #0			/* ponemos el valor 0 */
	bl digitalWrite		/* llamamos a la funcion digitalWrite */
						/* esperamos medio segundo */
						/* delay(int ms) */
	mov r0, #500		/* indicamos el numero de milisegundos */		
	bl delay			/* llamamos a la funcion delay */
						/* leemos el estado de los pulsadores */
						/* int digitalRead(int pin) */
	mov r0, #BUTTON1	/* indicamos el pin */
	bl digitalRead		/* llamamos a la funcion digitalRead */
	cmp r0, #0			/* vemos si el estado leido del pin es 0 */
	beq salir			/* si es 0, se ha pulsado y salimos */
	mov r0, #BUTTON2	/* indicamos el pin */
	bl digitalRead		/* llamamos a la funcion digitalRead */
	cmp r0, #0			/* vemos si el estado leido del pin es 0 */
	bne bucle			/* si no es 0, no se ha pulsado y volvemos a parpadear */

salir:					/* syscall exit(int status) */
	mov     r0, #0     	/* status -> 0 */
	mov     r7, #1     	/* exit es syscall #1 */
	swi     #0          /* llamada a syscall */
