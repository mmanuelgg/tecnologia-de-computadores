.data
p: .word 6

.text
.global main
main:
	push {lr}
	bl initBerry
	ldr r0, =p
	ldr r0, [r0]
	bl setLeds
	pop {lr}
	bx lr
