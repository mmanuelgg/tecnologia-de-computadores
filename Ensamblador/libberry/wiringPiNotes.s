.set C, 3822	@DO
.set Db, 3608	@RE Bemol
.set D, 3405	@RE
.set Eb, 3214	@MI Bemol
.set E, 3034	@MI
.set F, 2863	@FA
.set Gb, 2703	@SOL Bemol
.set G, 2551	@SOL
.set Ab, 2408	@LA Bemol
.set A, 2272	@LA
.set Bb, 2146	@SI Bemol
.set B, 2025	@SI
.set CP, 1911	@AGUDOS
.set DbP, 1803	
.set DP, 1702
.set EbP, 1607
.set EP, 1517
.set FP, 1432
.set GbP, 1351
.set GP, 1276
.set AbP, 1204
.set AP, 1136
.set BbP, 1072
.set BP, 1012
.set SILEN, 20  @silencio
/* Duraciones */
.set NG,  500		@Negra
.set CORP,  375		@Corchea punto
.set COR,    250	@Corchea
.set FUS,   125		@Fusa
.set BL, 1000		@Blanca
