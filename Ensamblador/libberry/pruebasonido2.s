.data
notes: .word FA, SOLs, LA, LAs, SI, DOH, DOHs, REH, REHs, MIH, FAH, FAHs, SOLH, SOLHs, LAH, SILEN, NG, CORP, COR, FUS, BL 
.include "NotasYDuraciones.s"

.text
.global main
main:
	push {lr}
	bl initBerry
	
	mov r0, #SI
	mov r1, #BL
	bl playNote
	
	mov r0, #LA
	mov r1, #NG
	bl playNote
	
	mov r0, #SI
	mov r1, #NG
	bl playNote
	
	mov r0, #LA
	mov r1, #BL
	bl playNote
	
	pop {lr}
	bx lr
