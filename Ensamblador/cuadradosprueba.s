.data
n:	.word 5

.text
.global main
main:
	ldr r0, =n
	ldr r0, [r0]
	push {lr}
	bl cuad
	pop {lr}
	bx lr
cuad:
	cmp r0, #0
	beq salir
	mul r1, r0, r0
	sub r0, r0, #1
	push {r1,lr}
	bl cuad
	pop {r1,lr}
	add r0, r0, r1
salir:	
	bx lr