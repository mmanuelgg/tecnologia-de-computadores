.data
dato1:	.word 5
dato2:	.word 3
max:	.word 0

.text
.global main
main:
	ldr r0, =dato1
	ldr r1, [r0]
	ldr r0, =dato2
	ldr r2, [r0]
	cmp r1, r2
	movgt r0, r1
	movle r0, r2
	ldr r3, =max
	str r0, [r3]
	bx lr