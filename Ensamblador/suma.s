.data
dato1:	.word 5
dato2:	.word 3
res:	.word 0

.text
.global main
main:
	ldr r0, =dato1
	ldr r1, [r0]
	ldr r0, =dato2
	ldr r2, [r0]
	add r3, r1, r2
	ldr r0, =res
	str r3, [r0]
	bx lr